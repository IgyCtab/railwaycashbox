﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using BusinessLayer.Response;
using BusinessLayer.Services;
using RailwayCashboxApp.ViewModel;

namespace RailwayCashboxApp.Controllers
{
    public class HomeController : Controller
    {
        TrainService trainService;

        public HomeController()
        {
            this.trainService = new TrainService();
        }

        public async Task<ActionResult> Index()
        {
            TrainViewModel train = new TrainViewModel();
            await train.LoadCarriageType();
            await train.LoadTrain();
            return View(train);
        }

        [HttpGet]

        public async Task<ActionResult> Info()
        {

            var id = Convert.ToInt32(Request.QueryString["id"]);
            TrainViewModel train = new TrainViewModel();
            await train.LoadCarriageType();
            train.Train=  await trainService.FindTrainById(id);
            return View(train);
          
        }

        [HttpPost]
        public async Task<ActionResult> Index(TrainViewModel model)
        {
            var train = new TrainResponse
            {
                Name = model.Name
            };
            await trainService.AddTrain(train);
            return RedirectToAction("Index", "Home");
        }
        [HttpPost]
        public async Task<ActionResult> DeleteTrain(int id)
        {

            await trainService.DeleteTrain(id);
            return RedirectToAction("Index", "Home");
        }
        [HttpPost]
        public async Task<ActionResult> AddCarriage(TrainViewModel model)
        {
            var carriage = new CarriageResponse
            {
                CarriageTypeId = model.CarriageTypeId,
                CountPlace = model.CountPlace,
                Price = model.Price,
                TrainsId = model.TrainsId
            };
            await trainService.AddCarriage(carriage);
            return RedirectToAction("Index", "Home");

        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }


        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}