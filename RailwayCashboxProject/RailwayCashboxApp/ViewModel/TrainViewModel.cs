﻿using BusinessLayer.Response;
using BusinessLayer.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace RailwayCashboxApp.ViewModel
{
    public class TrainViewModel
    {
        public string Name { get; set; }
        public int TrainsId { get; set; }
        public int CountPlace { get; set; }
        public int CarriageTypeId { get; set; }
        public int Price { get; set; }

        public TrainService service;
        public TrainResponse[] Trains { get; set; }
        public CarriageTypeResponse[]  CarriagesTypes{ get; set; }

        public TrainResponse Train { get; set; }
        public TrainViewModel()
        {
            service = new TrainService();
        }

        public async Task LoadTrain()
        {
            Trains = await service.ShowTrain();
        }

        public async Task LoadCarriageType()
        {
            CarriagesTypes= await service.ShowCarriagesType();
        }
    }
}