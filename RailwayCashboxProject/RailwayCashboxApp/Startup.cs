﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(RailwayCashboxApp.Startup))]
namespace RailwayCashboxApp
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
