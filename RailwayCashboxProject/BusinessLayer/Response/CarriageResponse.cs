﻿namespace BusinessLayer.Response
{
    public class CarriageResponse
    {
        public int CarriageTypeId { get; set; }
        public PlaceResponse[] Place { get; set; }
        public double Price { get; set; }
        public int TrainsId { get; set; }
        public int CountPlace { get; set; }
    }
}
