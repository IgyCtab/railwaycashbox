﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Response
{
   public class TrainResponse
    {
        public int TrainId { get; set; }
        public string Name { get; set; }
       public CarriageResponse[] CarriageResponses { get; set; }
    }
}
