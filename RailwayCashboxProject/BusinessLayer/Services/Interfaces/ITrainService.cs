﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLayer.Response;

namespace BusinessLayer.Services.Interfaces
{
   public interface ITrainService
   {
       Task AddTrain(TrainResponse train);
       Task<TrainResponse[]> ShowTrain();
        Task<CarriageTypeResponse[]> ShowCarriagesType();
        Task AddCarriage(CarriageResponse carriage);
       Task DeleteTrain(int id);
        Task<TrainResponse> FindTrainById(int id);

    }
}
