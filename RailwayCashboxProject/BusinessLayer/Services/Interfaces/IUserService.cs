﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLayer.Response;

namespace BusinessLayer.Services.Interfaces
{
   public interface IUserService
   {
       Task AddUser(UserResponse user);
   }
}
