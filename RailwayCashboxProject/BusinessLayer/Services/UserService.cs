﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLayer.Response;
using BusinessLayer.Services.Interfaces;
using DataAcessLayer.Entities;
using DataAcessLayer.Repositories.UserRepository;

namespace BusinessLayer.Services
{
    public class UserService:IUserService
    {
        UserWriteRepository userWriteRepository;

        public UserService()
        {
            this.userWriteRepository = new UserWriteRepository();
        }

        public async Task AddUser(UserResponse userResponse)
        {
            User user = new User()
            {
                FullName = userResponse.FullName,
                Email = userResponse.Email,
                Password = userResponse.Password
            };

           await userWriteRepository.AddUser(user);
        }
    }
}
