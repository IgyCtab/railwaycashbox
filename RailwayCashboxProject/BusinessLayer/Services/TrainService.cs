﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLayer.Response;
using BusinessLayer.Services.Interfaces;
using DataAcessLayer.Entities;
using DataAcessLayer.Repositories.TrainRepository;
using DataAcessLayer.Repositories.TrainRepository.Interfaces;
using DataAcessLayer.Repositories.CarriageTypeRepository;
using DataAcessLayer.Repositories.CarriageRepository;

namespace BusinessLayer.Services
{
    public class TrainService : ITrainService
    {
        TrainWriteRepository trainWriteRepository;
        TrainReadRepository trainReadRepository;
        CarriageTypeReadRepository carriageTypeReadRepository;
        CarriageWriteRepository carriageWriteRepository;
        public TrainService()
        {
            carriageTypeReadRepository = new CarriageTypeReadRepository();
            trainReadRepository = new TrainReadRepository();
            trainWriteRepository = new TrainWriteRepository();
            carriageWriteRepository = new CarriageWriteRepository();
        }


        public async Task AddTrain(TrainResponse trainResponse)
        {
            Train train = new Train()
            {
                Name = trainResponse.Name
            };
            await trainWriteRepository.AddTrain(train);
        }

        public async Task<TrainResponse[]> ShowTrain()
        {

            var train = await trainReadRepository.ShowTrain();

            return train.Select(x => new TrainResponse
            {
                TrainId = x.TrainId,
                Name = x.Name,
                CarriageResponses = x.Сarriages.Select(p => new CarriageResponse()
                {
                    Price = p.Price,
                    CarriageTypeId = p.СarriageTypeId,
                    Place = p.Places.Select(place => new PlaceResponse()
                    {
                        PlaceNumber = place.Number
                    }).ToArray()
                }).ToArray()
            }).ToArray();
        }

        public async Task<CarriageTypeResponse[]> ShowCarriagesType()
        {
            var carriage = await carriageTypeReadRepository.GetSarriageTypes();
            return carriage.Select(x => new CarriageTypeResponse
            {
                CarriageTypeId = x.СarriageTypeId,
                Name = x.Name
            }).ToArray();
        }

        public async Task AddCarriage(CarriageResponse carriage)
        {
            var trainsCarriage = new Carriage()
            {
                СarriageTypeId = carriage.CarriageTypeId,
                Price = carriage.Price,
                TrainId = carriage.TrainsId
            };
            trainsCarriage.Places =
                Enumerable.Range(1, carriage.CountPlace).Select(x => new Place
                {
                    Carriage = trainsCarriage,
                    Number = x
                }).ToList();
            await carriageWriteRepository.AddCarriage(trainsCarriage);
        }

        public async Task DeleteTrain(int id)
        {
          await  trainWriteRepository.DeleteTrain(id);
        }

        public async Task<TrainResponse> FindTrainById(int id)
        {
            var train = await trainReadRepository.FindTrainById(id);
            return new TrainResponse()
            {
                TrainId = train.TrainId,
                Name = train.Name,
                CarriageResponses = train.Сarriages.Select(p => new CarriageResponse()
                {
                    Price = p.Price,
                    CarriageTypeId = p.СarriageTypeId,
                    Place = p.Places.Select(place => new PlaceResponse()
                    {
                        PlaceNumber = place.Number
                    }).ToArray()
                }).ToArray()
            };

        }
    }
}
