﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(RailwayCashbox.Startup))]
namespace RailwayCashbox
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
