﻿using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace DataAcessLayer.Context
{
    public class ApplicationUser : IdentityUser
    {

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            return userIdentity;
        }
    }

    public class IdentityContext : IdentityDbContext<ApplicationUser>
    {
        public IdentityContext()
                 : base(@"data source=(LocalDb)\MSSQLLocalDB;
                        initial catalog=IdentityDataBase;
                        integrated security=True;MultipleActiveResultSets=True;
                        App=EntityFramework")
        {
        }

        public static IdentityContext Create()
        {
            return new IdentityContext();
        }
    }
}