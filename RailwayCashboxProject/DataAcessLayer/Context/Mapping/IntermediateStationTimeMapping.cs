﻿using DataAcessLayer.Entities;
using System.Data.Entity.ModelConfiguration;

namespace DataAcessLayer.Context.Mapping
{
    public class IntermediateStationTimeMapping : EntityTypeConfiguration<IntermediateStationTime>
    {
        public IntermediateStationTimeMapping()
        {
            HasKey(p => p.IntermediateStationTimeId);

            HasRequired(x => x.IntermediateStation)
                .WithMany(x => x.IntermediateStationTimes)
                .HasForeignKey(x => x.IntermediateStationId);

            Property(x => x.IntermediateStationTimeId).HasColumnName("intermediateStationTimeId");
            Property(x => x.IntermediateStationId).HasColumnName("intermediateStationId");
            Property(x => x.TimeDeparture).HasColumnName("timeDeparture");
            Property(x => x.TimeArrival).HasColumnName("timeArrival");
        }
    }
}
