﻿using DataAcessLayer.Entities;
using System.Data.Entity.ModelConfiguration;

namespace DataAcessLayer.Context.Mapping
{
    public class СarriageMapping : EntityTypeConfiguration<Carriage>
    {
        public СarriageMapping()
        {
            HasKey(p => p.СarriageId);

            HasRequired(p => p.Train)
                .WithMany(x => x.Сarriages)
                .HasForeignKey(x=>x.TrainId);

            HasRequired(p => p.CarriageType)
                .WithMany(x=>x.Сarriages)
                .HasForeignKey(x=>x.СarriageTypeId);

            Property(x => x.СarriageId).HasColumnName("carriageId");
            Property(x => x.TrainId).HasColumnName("trainId");
            Property(x => x.СarriageTypeId).HasColumnName("carriageTypeId");
            Property(x => x.Price).HasColumnName("price");
        }
    }
}
