﻿using DataAcessLayer.Entities;
using System.Data.Entity.ModelConfiguration;

namespace DataAcessLayer.Context.Mapping
{
    public class RouteMapping : EntityTypeConfiguration<Route>
    {
        public RouteMapping()
        {
            HasKey(p => p.RouteId);

            HasMany(x => x.ItermediateStation)
                .WithRequired(x => x.Route)
                .HasForeignKey(x=>x.RouteId);

            HasRequired(x => x.Train)
                .WithMany(x => x.Routes)
                .HasForeignKey(x=>x.TrainId);

            //HasRequired(x => x.StartStation)
            //    .WithMany(x => x.Routes)
            //    .HasForeignKey(x=>x.StartStationId);

            //HasRequired(x => x.EndStation)
            //    .WithMany(x => x.Routes)
            //    .HasForeignKey(x=>x.EndStationId);

            Property(x => x.RouteId).HasColumnName("routeId");
            Property(x => x.TrainId).HasColumnName("trainId");
            Property(x => x.StartStationId).HasColumnName("startStationId");
            Property(x => x.EndStationId).HasColumnName("endStationId");
          
        }
    }
}
