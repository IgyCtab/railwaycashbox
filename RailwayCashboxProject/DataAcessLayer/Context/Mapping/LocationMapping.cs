﻿using DataAcessLayer.Entities;
using System.Data.Entity.ModelConfiguration;

namespace DataAcessLayer.Context.Mapping
{
    public class LocationMapping : EntityTypeConfiguration<Location>
    {
        public LocationMapping()
        {
            HasKey(p => p.LocationId); 

            Property(x => x.LocationId).HasColumnName("locationId");
            Property(x => x.Latitude).HasColumnName("latitude");
            Property(x => x.Longitude).HasColumnName("longitude");
        }
      
    }
}
