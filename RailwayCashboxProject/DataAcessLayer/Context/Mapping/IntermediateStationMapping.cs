﻿using DataAcessLayer.Entities;
using System.Data.Entity.ModelConfiguration;


namespace DataAcessLayer.Context.Mapping
{
    public class IntermediateStationMapping: EntityTypeConfiguration<IntermediateStation>
    {
        public IntermediateStationMapping()
        {
            HasKey(p => p.IntermediateStationId);

            //HasRequired(x => x.StartStation)
            //    .WithMany(x=>x.IntermediateStations)
            //    .HasForeignKey(x=>x.StartStationId);

            //HasRequired(x => x.EndStation)
            //    .WithMany(x => x.IntermediateStations)
            //    .HasForeignKey(x=>x.EndStationId);

            Property(x => x.IntermediateStationId).HasColumnName("intermediateStationId");
            Property(x => x.StartStationId).HasColumnName("startStationId");
            Property(x => x.EndStationId).HasColumnName("endStationId");
            Property(x => x.TimeDeparture).HasColumnName("timeDeparture");
            Property(x => x.TimeArrival).HasColumnName("timeArrival");
        }
    }
}
