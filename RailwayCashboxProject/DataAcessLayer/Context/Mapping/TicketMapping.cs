﻿using DataAcessLayer.Entities;
using System.Data.Entity.ModelConfiguration;


namespace DataAcessLayer.Context.Mapping
{
    public class TicketMapping : EntityTypeConfiguration<Ticket>
    {
        public TicketMapping()
        {
            HasKey(p => p.TicketId);

            HasRequired(x => x.Route)
                .WithMany(x=>x.Tickets)
                .HasForeignKey(x=>x.RouteId);

            HasRequired(x => x.TypeTicket)
                .WithMany(x=>x.Tickets)
                .HasForeignKey(x=>x.TypeTicketId);

            Property(x => x.TicketId).HasColumnName("ticketId");
            Property(x => x.TypeTicketId).HasColumnName("typeTicketId");
            Property(x => x.RouteId).HasColumnName("routeId");
            Property(x => x.OrderId).HasColumnName("orderId");
            Property(x => x.TimeDeparture).HasColumnName("timeDeparture");
        }
    }
}
