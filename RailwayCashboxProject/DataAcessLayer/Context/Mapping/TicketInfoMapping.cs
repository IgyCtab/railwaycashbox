﻿using DataAcessLayer.Entities;
using System.Data.Entity.ModelConfiguration;

namespace DataAcessLayer.Context.Mapping
{
    public class TicketInfoMapping : EntityTypeConfiguration<TicketInfo>
    {
        public TicketInfoMapping()
        {
            HasKey(x => x.TicketInfoId);

            HasRequired(x => x.Place)
                .WithMany(x=>x.Tickets)
                .HasForeignKey(x=>x.PlaceId);

            HasRequired(x => x.Ticket)
                .WithOptional(x => x.TicketInfo);

            Property(x => x.TicketInfoId).HasColumnName("ticketInfoId");
            Property(x => x.PlaceId).HasColumnName("placeId");
        }
    }
}
