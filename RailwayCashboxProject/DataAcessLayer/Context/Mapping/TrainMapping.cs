﻿using DataAcessLayer.Entities;
using System.Data.Entity.ModelConfiguration;

namespace DataAcessLayer.Context.Mapping
{
    public class TrainMapping : EntityTypeConfiguration<Train>
    {
        public TrainMapping()
        {
            HasKey(p => p.TrainId);

            Property(x => x.TrainId).HasColumnName("trainId");
            Property(x => x.Name).HasColumnName("name");

        }
    }
}
