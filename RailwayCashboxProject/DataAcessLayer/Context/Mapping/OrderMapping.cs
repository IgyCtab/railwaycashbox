﻿using DataAcessLayer.Entities;
using System.Data.Entity.ModelConfiguration;

namespace DataAcessLayer.Context.Mapping
{
    public class OrderMapping : EntityTypeConfiguration<Order>
    {
        public OrderMapping()
        {
            HasKey(x => x.OrderId);

            HasMany(x => x.Tickets)
                .WithRequired(x=>x.Order)
                .HasForeignKey(x=>x.OrderId);

            Property(x => x.OrderId).HasColumnName("orderId");
            Property(x => x.UserId).HasColumnName("userId");
            Property(x => x.Date).HasColumnName("date");
        }
    }
}
