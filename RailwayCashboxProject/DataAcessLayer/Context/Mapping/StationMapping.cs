﻿using DataAcessLayer.Entities;
using System.Data.Entity.ModelConfiguration;

namespace DataAcessLayer.Context.Mapping
{
    public class StationMapping : EntityTypeConfiguration<Station>
    {
        public StationMapping()
        {
            HasKey(p => p.StationId);
            HasRequired(p => p.Location);

            Property(x => x.StationId).HasColumnName("stationId");
            Property(x => x.Name).HasColumnName("name");
            Property(x => x.LocationId).HasColumnName("locationId");
        }
    }
}
