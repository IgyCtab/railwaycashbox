﻿using DataAcessLayer.Entities;
using System.Data.Entity.ModelConfiguration;

namespace DataAcessLayer.Context.Mapping
{
    public class UserMapping : EntityTypeConfiguration<User>
    {
        public UserMapping()
        {
            HasKey(p => p.UserId);

            HasMany(p => p.Orders)
                .WithRequired(x=>x.User)
                .HasForeignKey(x=>x.UserId);

            Property(x => x.UserId).HasColumnName("userId");
            Property(x => x.FullName).HasColumnName("fullName");
            Property(x => x.Email).HasColumnName("email");
            Property(x => x.Password).HasColumnName("password");
        }
    }
}
