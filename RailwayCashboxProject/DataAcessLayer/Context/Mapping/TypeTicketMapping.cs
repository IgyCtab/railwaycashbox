﻿using DataAcessLayer.Entities;
using System.Data.Entity.ModelConfiguration;

namespace DataAcessLayer.Context.Mapping
{
    public class TypeTicketMapping : EntityTypeConfiguration<TypeTicket>
    {
        public TypeTicketMapping()
        {
            HasKey(p => p.TypeTicketId);

            Property(x => x.TypeTicketId).HasColumnName("typeTicketId");
            Property(x => x.Type).HasColumnName("type");
        }
    }
}
