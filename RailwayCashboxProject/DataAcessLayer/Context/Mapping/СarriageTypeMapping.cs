﻿using DataAcessLayer.Entities;
using System.Data.Entity.ModelConfiguration;

namespace DataAcessLayer.Context.Mapping
{
    public class СarriageTypeMapping : EntityTypeConfiguration<CarriageType>
    {
        public СarriageTypeMapping()
        {
            HasKey(p => p.СarriageTypeId); 

            Property(x => x.СarriageTypeId).HasColumnName("carriageTypeId");
            Property(x => x.Name).HasColumnName("name");
        }
    }
}
