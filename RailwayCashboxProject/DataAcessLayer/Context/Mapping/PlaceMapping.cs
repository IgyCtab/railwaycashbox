﻿using DataAcessLayer.Entities;
using System.Data.Entity.ModelConfiguration;

namespace DataAcessLayer.Context.Mapping
{
    public class PlaceMapping : EntityTypeConfiguration<Place>
    {
        public PlaceMapping()
        {
            HasKey(p => p.PlaceId);

            HasRequired(f => f.Carriage)
                .WithMany(f => f.Places)
                .HasForeignKey(x=>x.СarriageId);

            Property(x => x.PlaceId).HasColumnName("placeId");
            Property(x => x.Number).HasColumnName("number");
            Property(x => x.СarriageId).HasColumnName("carriageId");
        }
    }
}
