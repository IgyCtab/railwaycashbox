﻿using DataAcessLayer.Context.Mapping;
using DataAcessLayer.Entities;
using System.Data.Entity;

namespace DataAcessLayer.Context
{
    public class RailwayCashboxContext : DbContext
    {
        public DbSet<Carriage> Carriages { get; set; }
        public DbSet<CarriageType> CarriageTypes { get; set; }
        public DbSet<IntermediateStation> IntermediateStations { get; set; }
        public DbSet<IntermediateStationTime> IntermediateStationTimes { get; set; }
        public DbSet<Location> Locations { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Place> Places { get; set; }
        public DbSet<Route> Routes { get; set; }
        public DbSet<RouteTime> RouteTimes { get; set; }
        public DbSet<Station> Stations { get; set; }
        public DbSet<Ticket> Tickets { get; set; }
        public DbSet<TicketInfo> TicketInfos { get; set; }
        public DbSet<Train> Trains { get; set; }
        public DbSet<TypeTicket> TypeTickets { get; set; }
        public DbSet<User> Users { get; set; }
     

        public RailwayCashboxContext()
            
            : base(@"data source=(LocalDb)\MSSQLLocalDB;
                        initial catalog=RailwayCashboxDataBase;
                        integrated security=True;MultipleActiveResultSets=True;
                        App=EntityFramework")

        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            Database.SetInitializer(new DropCreateDatabaseIfModelChanges<RailwayCashboxContext>());

            base.OnModelCreating(modelBuilder);

            modelBuilder.Configurations.Add(new IntermediateStationMapping());
            modelBuilder.Configurations.Add(new LocationMapping());
            modelBuilder.Configurations.Add(new OrderMapping());
            modelBuilder.Configurations.Add(new PlaceMapping());
            modelBuilder.Configurations.Add(new RouteMapping());

            modelBuilder.Configurations.Add(new RouteTimeMapping());
            modelBuilder.Configurations.Add(new StationMapping());
            modelBuilder.Configurations.Add(new TicketMapping());
            modelBuilder.Configurations.Add(new TicketInfoMapping());
            modelBuilder.Configurations.Add(new TrainMapping());

            modelBuilder.Configurations.Add(new TypeTicketMapping());
            modelBuilder.Configurations.Add(new UserMapping());
            modelBuilder.Configurations.Add(new СarriageMapping());
            modelBuilder.Configurations.Add(new СarriageTypeMapping());
            modelBuilder.Configurations.Add(new IntermediateStationTimeMapping());
        }

    }
}
