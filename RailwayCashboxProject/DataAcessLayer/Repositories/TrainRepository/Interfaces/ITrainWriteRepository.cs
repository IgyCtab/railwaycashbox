﻿using System.Threading.Tasks;
using DataAcessLayer.Entities;

namespace DataAcessLayer.Repositories.TrainRepository.Interfaces
{
    public interface ITrainWriteRepository
    {
        Task AddTrain(Train train);
        Task DeleteTrain(int trainId);
    }
}
