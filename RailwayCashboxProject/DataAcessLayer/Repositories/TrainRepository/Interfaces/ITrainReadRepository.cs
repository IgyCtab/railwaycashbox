﻿using System.Threading.Tasks;
using DataAcessLayer.Entities;

namespace DataAcessLayer.Repositories.TrainRepository.Interfaces
{
   public interface ITrainReadRepository
   {
       Task<Train[]> ShowTrain();
        Task<Train> FindTrainById(int id);
   }
}
