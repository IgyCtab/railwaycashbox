﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using DataAcessLayer.Context;
using DataAcessLayer.Entities;
using DataAcessLayer.Repositories.TrainRepository.Interfaces;

namespace DataAcessLayer.Repositories.TrainRepository
{
    public class TrainReadRepository : ITrainReadRepository
    {
        RailwayCashboxContext context;

        public TrainReadRepository()
        {
            this.context = new RailwayCashboxContext();
        }

        public Task<Train> FindTrainById(int id)
        {
           return  context.Trains.FirstOrDefaultAsync(x => x.TrainId == id);
        }

        public Task<Train[]> ShowTrain()
        {
            return context.Trains.ToArrayAsync();
        }
    }
}
