﻿using System.Linq;
using System.Threading.Tasks;
using DataAcessLayer.Context;
using DataAcessLayer.Entities;
using DataAcessLayer.Repositories.TrainRepository.Interfaces;

namespace DataAcessLayer.Repositories.TrainRepository
{
    public class TrainWriteRepository : ITrainWriteRepository
    {
        RailwayCashboxContext context;

        public TrainWriteRepository()
        {
            this.context = new RailwayCashboxContext();
        }

        public Task AddTrain(Train train)
        {
            context.Trains.Add(train);
            return context.SaveChangesAsync();
        }

        public Task DeleteTrain(int trainId)
        {
            var train = context.Trains.FirstOrDefault(x => x.TrainId == trainId);
            context.Trains.Remove(train);
            return context.SaveChangesAsync(); 
        }
    }
}
