﻿using System.Data.Entity;
using System.Threading.Tasks;
using DataAcessLayer.Context;
using DataAcessLayer.Entities;
using DataAcessLayer.Repositories.CarriageTypeRepository.Interfaces;

namespace DataAcessLayer.Repositories.CarriageTypeRepository
{
  public  class CarriageTypeReadRepository: ICarriageTypeReadRepository
  {
      RailwayCashboxContext context;

      public CarriageTypeReadRepository()
      {
          this.context = new RailwayCashboxContext();
      }

      public Task<CarriageType[]> GetSarriageTypes()
      {
          return context.CarriageTypes.ToArrayAsync();
      }
    }
}
