﻿using System.Threading.Tasks;
using DataAcessLayer.Context;
using DataAcessLayer.Entities;
using DataAcessLayer.Repositories.CarriageTypeRepository.Interfaces;

namespace DataAcessLayer.Repositories.CarriageTypeRepository
{
  public  class CarriageTypeWriteRepository: ICarriageTypeWriteRepository
    {
        RailwayCashboxContext contex;

        public CarriageTypeWriteRepository(RailwayCashboxContext contex)
        {
            this.contex = contex;
        }

        public Task AddCarriageType(CarriageType carriageType)
        {
            contex.CarriageTypes.Add(carriageType);
            return contex.SaveChangesAsync();
        }
    }
}
