﻿using System.Threading.Tasks;
using DataAcessLayer.Entities;

namespace DataAcessLayer.Repositories.CarriageTypeRepository.Interfaces
{
 public   interface ICarriageTypeReadRepository
 {
     Task<CarriageType[]> GetSarriageTypes();
 }
}
