﻿using System.Threading.Tasks;
using DataAcessLayer.Entities;

namespace DataAcessLayer.Repositories.CarriageTypeRepository.Interfaces
{
   public interface ICarriageTypeWriteRepository
   {
       Task AddCarriageType(CarriageType carriageType);
   }
}
