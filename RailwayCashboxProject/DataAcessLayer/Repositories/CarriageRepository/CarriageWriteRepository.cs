﻿using System.Threading.Tasks;
using DataAcessLayer.Context;
using DataAcessLayer.Entities;
using DataAcessLayer.Repositories.CarriageRepository.Interfaces;

namespace DataAcessLayer.Repositories.CarriageRepository
{
  public  class CarriageWriteRepository: ICarriageWriteRepository
    {
        RailwayCashboxContext context;

        public CarriageWriteRepository()
        {
            this.context = new RailwayCashboxContext();
        }

        public Task AddCarriage(Carriage carriage)
        {
            context.Carriages.Add(carriage);
            return context.SaveChangesAsync();
        }
    }
}
