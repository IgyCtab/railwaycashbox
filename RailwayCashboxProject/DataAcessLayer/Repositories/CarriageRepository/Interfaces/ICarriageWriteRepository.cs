﻿using System.Threading.Tasks;
using DataAcessLayer.Entities;

namespace DataAcessLayer.Repositories.CarriageRepository.Interfaces
{
   public interface ICarriageWriteRepository
   {
       Task AddCarriage(Carriage carriage);
   }
}
