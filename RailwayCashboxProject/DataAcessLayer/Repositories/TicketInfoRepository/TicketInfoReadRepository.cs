﻿using DataAcessLayer.Repositories.TicketInfoRepository.Interfaces;

namespace DataAcessLayer.Repositories.TicketInfoRepository
{
    interface TicketInfoReadRepository: ITicketInfoReadRepository
    {
    }
}
