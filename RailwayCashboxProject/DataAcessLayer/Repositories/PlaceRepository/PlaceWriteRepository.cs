﻿using System.Threading.Tasks;
using DataAcessLayer.Context;
using DataAcessLayer.Entities;
using DataAcessLayer.Repositories.PlaceRepository.Interfaces;

namespace DataAcessLayer.Repositories.PlaceRepository
{
  public class PlaceWriteRepository: IPlaceWriteRepository
  {
      RailwayCashboxContext context;

      public PlaceWriteRepository(RailwayCashboxContext context)
      {
          this.context = context;
      }

      public Task AddPlace(Place place)
      {
          context.Places.Add(place);
          return context.SaveChangesAsync();
      }
    }
}
