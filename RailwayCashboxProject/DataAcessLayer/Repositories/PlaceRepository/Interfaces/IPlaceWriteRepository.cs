﻿using System.Threading.Tasks;
using DataAcessLayer.Entities;

namespace DataAcessLayer.Repositories.PlaceRepository.Interfaces
{
  public  interface IPlaceWriteRepository
  {
      Task AddPlace(Place place);
  }
}
