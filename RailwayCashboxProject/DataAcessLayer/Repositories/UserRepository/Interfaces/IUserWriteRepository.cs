﻿using DataAcessLayer.Entities;
using System.Threading.Tasks;

namespace DataAcessLayer.Repositories.UserRepository.Interfaces
{
    public interface IUserWriteRepository
    {
        Task AddUser(User user);
    }
}
