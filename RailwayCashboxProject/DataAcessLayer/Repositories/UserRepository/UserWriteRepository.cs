﻿using System;
using System.Threading.Tasks;
using DataAcessLayer.Entities;
using DataAcessLayer.Repositories.UserRepository.Interfaces;
using DataAcessLayer.Context;

namespace DataAcessLayer.Repositories.UserRepository
{
   public class UserWriteRepository : IUserWriteRepository
    {
        RailwayCashboxContext context;

        public UserWriteRepository()
        {
            this.context = new RailwayCashboxContext();
        }

        public Task AddUser(User user)
        {
            context.Users.Add(user);
           return context.SaveChangesAsync();
        }
    }
}
