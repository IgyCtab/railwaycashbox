﻿using System.Collections.Generic;

namespace DataAcessLayer.Entities
{
    public class Station
    {
        public int StationId { get; set; }
        public string Name { get; set; }
        public int LocationId { get; set; }
        public virtual Location Location { get; set; }

        public virtual ICollection<Route> Routes { get; set; }
        public virtual ICollection<IntermediateStation> IntermediateStations { get; set; }
    }
}
