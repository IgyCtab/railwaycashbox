﻿using System.Collections.Generic;

namespace DataAcessLayer.Entities
{
    public class Carriage
    {
        public int СarriageId { get; set; }
        public int TrainId { get; set; }
        public virtual Train Train { get; set; }
        public int СarriageTypeId { get; set; }
        public virtual CarriageType CarriageType { get; set; }
        public double Price { get; set; }

        public virtual ICollection<Place> Places { get; set; }
    }
}