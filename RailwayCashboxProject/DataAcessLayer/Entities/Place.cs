﻿using System.Collections.Generic;

namespace DataAcessLayer.Entities
{
    public class Place
    {
        public int PlaceId { get; set; }
        public int Number { get; set; }
        public int СarriageId { get; set; }
        public virtual Carriage Carriage { get; set; }

        public virtual ICollection<TicketInfo> Tickets { get; set; }

    }
}