﻿using System;

namespace DataAcessLayer.Entities
{
    public class Ticket
    {
        public int TicketId { get; set; }
        public int OrderId { get; set;}
        public virtual Order Order { get; set; }
        public int TypeTicketId { get; set; }
        public virtual TypeTicket TypeTicket { get; set; }
        public int RouteId { get; set; }
        public virtual Route Route { get; set; }
        public DateTime TimeDeparture { get; set; }

        public int TicketInfoId { get; set; }
        public virtual TicketInfo TicketInfo { get; set; }
    }
}