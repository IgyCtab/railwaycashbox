﻿namespace DataAcessLayer.Entities
{
    public class TicketInfo
    {
        public int TicketInfoId { get; set; }
        public int TicketId { get; set; }
        public virtual Ticket Ticket { get; set; }
        public int PlaceId { get; set; }
        public virtual Place Place { get; set; }
    }
}
