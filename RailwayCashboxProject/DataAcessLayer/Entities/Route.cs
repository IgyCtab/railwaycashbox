﻿using System.Collections.Generic;

namespace DataAcessLayer.Entities
{
    public class Route
    {
        public int RouteId { get; set; }
        public int StartStationId { get; set; }
        public virtual Station StartStation { get; set; }
        public int EndStationId { get; set; }
        public virtual Station EndStation { get; set; }
        public int TrainId { get; set; }
        public virtual Train Train { get; set; }
   
        public virtual ICollection<IntermediateStation> ItermediateStation { get; set; }
        public virtual ICollection<Ticket> Tickets { get; set; }
        public virtual ICollection<RouteTime> RouteTimes { get; set; }

    }
}
