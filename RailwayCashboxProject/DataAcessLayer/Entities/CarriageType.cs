﻿using System.Collections.Generic;

namespace DataAcessLayer.Entities
{
    public class CarriageType
    {
        public int СarriageTypeId { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Carriage> Сarriages { get; set; }
    }
}
