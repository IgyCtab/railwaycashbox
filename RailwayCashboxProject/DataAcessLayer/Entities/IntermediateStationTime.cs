﻿using System;

namespace DataAcessLayer.Entities
{
    public class IntermediateStationTime
    {
        public int IntermediateStationTimeId { get; set; }
        public int IntermediateStationId { get; set; }
        public virtual IntermediateStation IntermediateStation { get; set; }
        public DateTime TimeDeparture { get; set; }
        public DateTime TimeArrival { get; set; }

    }
}
