﻿using System;

namespace DataAcessLayer.Entities
{
    public class RouteTime
    {
        public int RouteTimeId { get; set; }
        public int RouteId { get; set; }
        public virtual Route Route { get; set; }
        public DateTime TimeDeparture { get; set; }
        public DateTime TimeArrival { get; set; }
    }
}
  