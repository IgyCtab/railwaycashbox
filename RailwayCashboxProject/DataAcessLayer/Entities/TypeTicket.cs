﻿using System.Collections.Generic;

namespace DataAcessLayer.Entities
{
    public class TypeTicket
    {
        public int TypeTicketId { get; set; }
        public string Type { get; set; }

        public virtual ICollection<Ticket> Tickets { get; set; }
    }
}