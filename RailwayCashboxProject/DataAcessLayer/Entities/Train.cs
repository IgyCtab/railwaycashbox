﻿using System.Collections.Generic;

namespace DataAcessLayer.Entities
{
    public class Train
    {
        public int TrainId { get; set; }
        public string Name { get; set; }
       
        public virtual ICollection<Carriage> Сarriages { get; set; }
        public virtual ICollection<Route> Routes { get; set; }

    }
}
