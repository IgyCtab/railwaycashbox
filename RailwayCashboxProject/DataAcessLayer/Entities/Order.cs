﻿using System;
using System.Collections.Generic;

namespace DataAcessLayer.Entities
{
    public class Order
    {
        public int OrderId { get; set; }
        public int UserId { get; set; }
        public virtual User User { get; set; }
        public DateTime Date { get; set; }

        public ICollection<Ticket> Tickets { get; set; }

        //public Order()
        //{
        //    var train = new Train();
        //    foreach (var item in train.Carriage)
        //    {
        //        foreach (var place in item.Places)
        //        {
        //            FreePlace(item.СarriageId, place.Number);
        //        }
        //    }
        
        //}
        //public bool FreePlace(int carriageId, int placeNumber)
        //{
        //    List<Order> ticket = new List<Order>();
        //    var result = new Ticket();
        //    foreach (var item in ticket.Where(x => x.Date == DateTime.Now))
        //    {
        //      result=  item.Tickets.
        //            FirstOrDefault(x => x.TicketInfo.СarriageId == carriageId 
        //            && x.TicketInfo.Place.Number.Equals(placeNumber));
        //    }
        //  return  result.Route == null ? true : false;
        //}
    }
}