﻿using System;
using System.Collections.Generic;

namespace DataAcessLayer.Entities
{
    public class IntermediateStation
    {
        public int IntermediateStationId { get; set; }
        public int RouteId { get; set; }
        public virtual Route Route { get; set; }
        public int StartStationId { get; set; }
        public virtual Station StartStation { get; set; }
        public int EndStationId { get; set; }
        public virtual Station EndStation { get; set; }
        public DateTime TimeDeparture { get; set; }
        public DateTime TimeArrival { get; set; }

        public virtual ICollection<IntermediateStationTime> IntermediateStationTimes { get; set; }

    }
}